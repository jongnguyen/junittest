import org.junit.jupiter.api.Assertions;

public class Test {
	@org.junit.jupiter.api.Test
	public void isNotException() {
		TestClass testData = new TestClass(50, 25);
		Assertions.assertEquals(testData.getSumPrice(), 1250);
	}
}
